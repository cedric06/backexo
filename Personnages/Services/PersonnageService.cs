﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Services
{
    using Models;
    public interface PersonnageService
    {
        public IEnumerable<Personnage> TrouverTout();
        public Personnage TrouverUn(int id);
        public Personnage Modifier(Personnage p);
        public Personnage Save(Personnage p);
        public void Supprimer(int id);
    }
}

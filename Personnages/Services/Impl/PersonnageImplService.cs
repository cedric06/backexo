﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Services.Impl
{
    using Personnages.Models;
    using Repositories;
    public class PersonnageImplService : PersonnageService
    {
        //création d'un attribut pour récuperer les informations de personnageRepository
        private PersonnageRepository repository;

        //Création d'un constructeur 
        public PersonnageImplService(PersonnageRepository repository)
        {
            this.repository = repository;
        }

        //Méthode qui va chercher la méthode modifier dans repository
        public Personnage Modifier(Personnage p)
        {
            return this.repository.UpDate(p);
        }

        //Méthode qui va chercher la methode save dans repository
        public Personnage Save(Personnage p)
        {
            return this.repository.Save(p);
        }

        //Méthode qui va chercher la methode supprimer dans repository
        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }

        //Méthode qui va chercher la methode trouverTout dans repository
        public IEnumerable<Personnage> TrouverTout()
        {
            return this.repository.FindAll();
        }

        //Méthode qui va chercher la methode trouverUn dans repository
        public Personnage TrouverUn(int id)
        {
            return this.repository.FindByID(id);
        }
    }
}

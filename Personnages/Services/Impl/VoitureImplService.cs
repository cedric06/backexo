﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Services.Impl
{
    using Personnages.Models;
    using Repositories;

    public class VoitureImplService : VoitureService
    {
        //création d'un attribut pour récuperer les informations de voitureRepository
        private VoitureRepository repository;

        public VoitureImplService(VoitureRepository repository)
        {
            this.repository = repository;
        }

        //Méthode qui va chercher la methode modifier dans repository
        public Voiture Modifier(Voiture v)
        {
            return this.repository.UpDate(v);
        }

        //Méthode qui va chercher la methode save dans repository
        public Voiture Save(Voiture v)
        {
            return this.repository.Save(v);
        }


        //Méthode qui va chercher la methode supprimer dans repository
        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }

        //Méthode qui va chercher la methode trouverTout dans repository
        public IEnumerable<Voiture> TrouverTout()
        {
            return this.repository.FindAll();
        }

        //Méthode qui va chercher la methode TrouverUn dans repository
        public Voiture TrouverUn(int id)
        {
            return this.repository.FindByID(id);
        }
    }
}

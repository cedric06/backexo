﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Personnages.Controllers
{
    using Models;
    using Services;

    [Route("voitures")]
    [ApiController]
    public class VoitureController : ControllerBase
    {
        //création d'un attribut pour récuperer les informations de voitureService
        private VoitureService service;

        public VoitureController(VoitureService service)
        {
            this.service = service;
        }

        //Requète qui enregistre un personnage et renvoie une erreur si la requète est fausse
        [HttpPost]
        [Route("")]
        public IActionResult Save([FromBody] Voiture v)
        {
            try
            {
                return Ok(this.service.Save(v));
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Requète qui recherche tous les personnages et renvoie une erreur si la requète est fausse
        [HttpGet]
        [Route("")]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(this.service.TrouverTout());
            }catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Requète qui recherche un personnage et renvoie une erreur si la requète est fausse
        [HttpGet]
        [Route("{id}")]
        public IActionResult FindByID(int id)
        {
            try
            {
                return Ok(this.service.TrouverUn(id));
            } catch (IndexOutOfRangeException e)
            {
                return NotFound(e.Message);
            }
        }

        //Requète qui modifie un personnage et renvoie une erreur si la requète est fausse
        public IActionResult UpDate([FromBody] Voiture v)
        {
            try
            {
                return Ok(this.service.Modifier(v));
            } catch (IndexOutOfRangeException e)
            {
                return NotFound(e.Message);
            }
        }

        //Requète qui supprime un personnage et renvoie une erreur si la requète est fausse
        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.service.Supprimer(id);
                return Ok();
            } catch (IndexOutOfRangeException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
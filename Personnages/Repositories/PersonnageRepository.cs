﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Repositories
{
    using Models;
    public interface PersonnageRepository
    {
        public Personnage Save(Personnage p);
        public IEnumerable<Personnage> FindAll();
        public Personnage FindByID(int id);
        public Personnage UpDate(Personnage p);
        public void Delete(int id);
        public void Delete(Personnage p);
    }
}

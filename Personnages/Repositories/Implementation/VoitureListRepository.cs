﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Repositories.Implementation
{
    using Models;

    public class VoitureListRepository : VoitureRepository
    {
        //Création d'une liste de voitures
        private List<Voiture> voitures = new List<Voiture>();

        //Méthode pour supprimer une voiture dans la liste
        public void Delete(int id)
        {
            this.voitures[id] = null;
        }

        //Méthode pour supprimer une voiture dans la liste
        public void Delete(Voiture v)
        {
            this.voitures[v.Id] = null;
        }

        //Méthode pour trouver toutes les voiture dans la liste
        public IEnumerable<Voiture> FindAll()
        {
            return this.voitures.Where(v => v != null);
        }

        //Méthode pour trouver une voiture dans la liste
        public Voiture FindByID(int id)
        {
            return this.voitures[id];
        }

        //Méthode pour enregistrer une voiture dans la liste
        public Voiture Save(Voiture v)
        {
            v.Id = this.voitures.Count();
            this.voitures.Add(v);
            return v;
        }

        //Méthode pour modifier une voiture dans la liste
        public Voiture UpDate(Voiture v)
        {
            this.voitures[v.Id] = v;
            return v;
        }
    }
}

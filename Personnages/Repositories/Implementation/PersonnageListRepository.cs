﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Personnages.Repositories.Implementation
{
    using Models;
    public class PersonnageListRepository : PersonnageRepository
    {
        //Création d'une liste de personnages
        private List<Personnage> personnages = new List<Personnage>();

        //Méthode pour supprimer un personnage dans la liste
        public void Delete(int id)
        {
            this.personnages[id] = null;
        }

        //Méthode pour supprimer une donnée
        public void Delete(Personnage p)
        {
            this.personnages[p.Id] = null;
        }

        //Méthode pour trouver tous les personnages dans la liste
        public IEnumerable<Personnage> FindAll()
        {
            return this.personnages.Where(p => p != null);
        }

        //Méthode pour trouver un personnage dans la liste
        public Personnage FindByID(int id)
        {
            return this.personnages[id];
        }

        //Méthdoe pour enregistrer un nouveau personnage dans la liste
        public Personnage Save(Personnage p)
        {
            p.Id = this.personnages.Count();
            this.personnages.Add(p);
            return p;
        }

        //Méthode pour modifier un personnage dans la liste
        public Personnage UpDate(Personnage p)
        {
            this.personnages[p.Id] = p;
            return p;
        }
    }
}
